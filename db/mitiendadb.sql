CREATE TABLE `category`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `order`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `provider_id` int NOT NULL,
  `order_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime NULL,
  `update_at` datetime NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `order_detail`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `product_id` int NOT NULL,
  `quantity` int NOT NULL,
  `cost` decimal(10, 2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unique_product_id`(`product_id`) USING BTREE
);

CREATE TABLE `product`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL,
  `deleted_at` datetime NULL,
  PRIMARY KEY (`id`),
  CHECK (price>0)
);

CREATE TABLE `provider`  (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `order` ADD CONSTRAINT `order_detail_id_order` FOREIGN KEY (`id`) REFERENCES `order_detail` (`order_id`);
ALTER TABLE `order` ADD CONSTRAINT `provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`);
ALTER TABLE `order_detail` ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);
ALTER TABLE `product` ADD CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

